from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect, HttpResponse, render_to_response
import hashlib
import urllib2
from config import *
import simplejson


# View Method -----------------------------------------------------------------------------------------------------
def entry(request):
    if request.GET:
        code = request.GET.get('code')
        get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?' \
                        'appid=' + APPID + \
                        '&secret=' + SECRETID + \
                        '&code=' + code +  \
                        '&grant_type=authorization_code'
        token_dict = get_json_from_url(get_token_url)
        if 'errcode' in token_dict:
            return render_to_response('wechat/error.html', token_dict)
        get_user_info_url = 'https://api.weixin.qq.com/sns/userinfo?' \
                            'access_token='+ token_dict['access_token'] + \
                            '&openid='+ token_dict['openid'] + \
                            '&lang=zh_CN'
        user_info_dict = get_json_from_url(get_user_info_url)
        if 'errcode' in user_info_dict:
            return render_to_response('wechat/error.html', user_info_dict)
        return render_to_response('wechat/userinfo.html', user_info_dict)


def wechat(request):
    if validate(request):
        return HttpResponse(request.REQUEST.get('echostr', ''))

# Utility Method -----------------------------------------------------------------------------------------------------
def validate(request):
    token = 'tongtianqi'
    signature = request.REQUEST.get('signature', '')
    timestamp = request.REQUEST.get('timestamp', '')
    nonce = request.REQUEST.get('nonce',  '')
    tmp_str = hashlib.sha1(''.join(sorted([token, timestamp, nonce]))).hexdigest()
    if tmp_str == signature:
        return True
    return False


def get_json_from_url(url):
    request = urllib2.Request(url)
    respond = urllib2.urlopen(request)
    dic = simplejson.load(respond)
    return dic


# Test Method -----------------------------------------------------------------------------------------------------
def error(request):
    context = {'errcode': 123409, 'errmsg': 'some error!'}
    return render_to_response('wechat/error.html', context)