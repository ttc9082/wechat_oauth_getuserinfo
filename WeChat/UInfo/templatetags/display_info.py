# coding: utf-8
from django import template

# Create your views here.
# This is a filter, when loaded, using functions below in templates are enabled.

register = template.Library()

@register.filter
def sex(value):
    if value == 1:
        return '男'
    elif value == 2:
        return '女'
    elif value == 0:
        return '未知'
    else:
        return '错误' + str(value)

@register.filter
def avatar(url):
    if url:
        return url
    else:
        return "http://www.jf258.com/uploads/2014-03-30/233454270.jpg"

@register.filter
def privilege(list):
    if list:
        return ' '.join(list)
    else:
        return '无'