from django.db import models

# Create your models here.


class User_info(models.Model):
    openid = models.CharField(max_length=500)
    refresh_token = models.CharField(max_length=500)
    time = models.DateTimeField()

    def __unicode__(self):
        return self.openid