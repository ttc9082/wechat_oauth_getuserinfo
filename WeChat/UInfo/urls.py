from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^entry$', 'UInfo.views.entry'),
    url(r'^$', 'UInfo.views.wechat'),
    url(r'^error', 'UInfo.views.error'),
    # url(r'^account/reg/$', 'pin.views.reg'),
)
