实现微信登录，即手机微信访问某个页面，显示登录授权页，之后页面显示用户的个人信息。
Demo：

1.使用微信关注该公共账号（10月9日后失效）
![RC.jpeg](https://bitbucket.org/repo/qnxAXX/images/2679090181-RC.jpeg)

2.在微信对话中点击链接： [https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx908b79041f834598&redirect_uri=http://pin.tongtianqi.com/wechat/entry&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect](https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx908b79041f834598&redirect_uri=http://pin.tongtianqi.com/wechat/entry&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect)

3.点击允许授权
![2009737674.jpg](https://bitbucket.org/repo/qnxAXX/images/1325849784-2009737674.jpg)

4.获取信息页面
![438171868.jpg](https://bitbucket.org/repo/qnxAXX/images/1216440640-438171868.jpg)

5.经对比信息无误
![1120504185.jpg](https://bitbucket.org/repo/qnxAXX/images/975224184-1120504185.jpg)